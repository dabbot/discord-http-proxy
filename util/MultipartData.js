// The MIT License (MIT)
//
// Copyright (c) 2016-2018 abalabahaha
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

"use strict";

class MultipartData {
    constructor() {
        this.boundary = "----------------Eris";
        this.bufs = [];
    }

    attach(fieldName, data, filename) {
        if(data === undefined) {
            return;
        }
        let str = "\r\n--" + this.boundary + "\r\nContent-Disposition: form-data; name=\"" + fieldName + "\"";
        if(filename) {
            str += "; filename=\"" + filename + "\"";
        }
        if(data instanceof Buffer) {
            str +="\r\nContent-Type: application/octet-stream";
        } else if(typeof data === "object") {
            str +="\r\nContent-Type: application/json";
            data = Buffer.from(JSON.stringify(data));
        } else {
            data = Buffer.from("" + data);
        }
        this.bufs.push(Buffer.from(str + "\r\n\r\n"));
        this.bufs.push(data);
    }

    finish() {
        this.bufs.push(Buffer.from("\r\n--" + this.boundary + "--"));
        return this.bufs;
    }
}

module.exports = MultipartData;
