FROM node:11.2.0-alpine

WORKDIR /app

COPY ./errors ./errors
COPY ./util ./util
COPY ./*.js ./
COPY ./package.json ./package.json

ENTRYPOINT node ./index.js
