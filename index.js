const RequestHandler = require('./RequestHandler');
const http = require('http');

const PORT = parseInt(process.env['PORT'] || 80);

const handler = new RequestHandler();

function main() {
  http.createServer((req, res) => httpHandler(req, res)).listen(PORT);
  console.log('listening');
}

async function httpHandler(req, res) {
  let body = [];
  let erisResponse = "";
  req.on('data', chunk => body.push(chunk));
  req.on('end', async () => {
    body = Buffer.concat(body).toString();
    body = JSON.parse(body);
    try {
      erisResponse = await handler.request(req.method, req.url, req.headers.authorization, body);
      res.writeHead(200, {'Content-Type': 'application/json'});
      res.end(JSON.stringify(erisResponse));
    } catch(e) {
      res.statusCode = e.statusCode;
      res.body = e.body;
      res.end();
    }
  });
}

main();
